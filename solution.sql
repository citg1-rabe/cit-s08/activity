--a. find all artists that ahs letter D in its name

SELECT * FROM artists WHERE name LIKE "%D%";
SELECT * FROM artists WHERE name LIKE "%d%";
-- b. Find all songs that has length of less than 230

SELECT * FROM songs WHERE length <230;

-- c. join albums and songs table(show album name, song name, song length.
SELECT album_title, song_name, length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- d. join the artists and albums table  (find all albums that has letter a in its name).
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%_A%";

SELECT * FROM artists
		JOIN albums ON artists.id = albums.artist_id;	

-- e. sort the albums in Z-A (show only the first 4 records)

SELECT * FROM albums ORDER BY  album_title DESC LIMIT 4;

-- f. Join the albums and songs table. (Sort albums from Z-A and sort songs from A-Z)
SELECT * FROM albums
 	JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;


